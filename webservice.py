from flask import Flask
from flask import request
from pymongo import MongoClient
from bson.json_util import dumps
import json
import pymongo


app = Flask(__name__)

@app.route("/BTC-AEON", methods = ['GET'])
def BTCAEON():
    try:
        myclient = pymongo.MongoClient("mongodb://localhost:27017/")
        mydb = myclient["BTC"]
        mycol = mydb["AEON"] 
        for x in mycol.find():
            print(x)    
        return dumps({'BTC-AEON': str(x)}) 
    except Exception as e:
        return dumps({'error' : str(e)})
@app.route("/BTC-BTCP", methods = ['GET'])
def BTCBTCP():
    try:
        myclient = pymongo.MongoClient("mongodb://localhost:27017/")
        mydb = myclient["BTC"]
        mycol = mydb["BTCP"] 
        for x in mycol.find():
            print(x)    
        return dumps({'BTC-BTCP': str(x)}) 
    except Exception as e:
        return dumps({'error' : str(e)})
if __name__ == '__main__':
    app.run(debug=True)
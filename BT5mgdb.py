import os, sys
import time
import urllib.request   
import json
import datetime
from pymongo import MongoClient
def btceGetTicker(pairs):

    try:
        ticker_raw = urllib.request.urlopen("https://tradeogre.com/api/v1/ticker/"+ pairs)
        ticker_encoding = ticker_raw.info().get_content_charset('utf-8')
        ticker_json = json.loads(ticker_raw.read().decode(ticker_encoding))
        return ticker_json
    except Exception as e:
        print("Get ticker failed: ", e)
    return None

def main():
    AEON  = btceGetTicker("BTC-AEON") 
    BTCP  = btceGetTicker("BTC-BTCP") 
    COAL  = btceGetTicker("BTC-COAL")  
    DASH  = btceGetTicker("BTC-DASH")
    DNR   = btceGetTicker("BTC-DNR")
    DOGE  = btceGetTicker("BTC-DOGE")
    ETN   = btceGetTicker("BTC-ETN")
    FBF   = btceGetTicker("BTC-FBF")
    GRFT  = btceGetTicker("BTC-GRFT")
    IPBC  = btceGetTicker("BTC-IPBC")
    IRD   = btceGetTicker("BTC-IRD")
    ITNS  = btceGetTicker("BTC-ITNS")
    KRB   = btceGetTicker("BTC-KRB")
    LTC   = btceGetTicker("BTC-LTC")
    LUX   = btceGetTicker("BTC-LUX")
    NAH   = btceGetTicker("BTC-NAH")
    NBR   = btceGetTicker("BTC-NBR")
    PCN   = btceGetTicker("BTC-PCN")
    PLURA = btceGetTicker("BTC-PLURA")
    SUMO  = btceGetTicker("BTC-SUMO")
    TRTL  = btceGetTicker("BTC-TRTL")
    WAE   = btceGetTicker("BTC-WAE")
    XAO   = btceGetTicker("BTC-XAO")
    XMR   = btceGetTicker("BTC-XMR")
    XTL   = btceGetTicker("BTC-XTL")
    XUN   = btceGetTicker("BTC-XUN")
    XVG   = btceGetTicker("BTC-XVG")
    dbClientConnection = MongoClient('localhost', 27017)
    db = dbClientConnection["BTC"]
    entry = db["AEON"]
    entry.insert_one(AEON)
    entry = db["BTCP"]
    entry.insert_one(BTCP)
    entry = db["COAL"]
    entry.insert_one(COAL)
    entry = db["DASH"]
    entry.insert_one(DASH)
    entry = db["DNR"]
    entry.insert_one(DNR)
    entry = db["DOGE"]
    entry.insert_one(DOGE)
    entry = db["ETN"]
    entry.insert_one(ETN)
    entry = db["FBF"]
    entry.insert_one(FBF)
    entry = db["GRFT"]
    entry.insert_one(GRFT)
    entry = db["IPBC"]
    entry.insert_one(IPBC)
    entry = db["IRD"]
    entry.insert_one(IRD)
    entry = db["ITNS"]
    entry.insert_one(ITNS)
    entry = db["KRB"]
    entry.insert_one(KRB)
    entry = db["LTC"]
    entry.insert_one(LTC)
    entry = db["LUX"]
    entry.insert_one(LUX)
    entry = db["NAH"]
    entry.insert_one(NAH)
    entry = db["NBR"]
    entry.insert_one(NBR)
    entry = db["PCN"]   
    entry.insert_one(PCN)
    entry = db["PLURA"]
    entry.insert_one(PLURA)
    entry = db["SUMO"]
    entry.insert_one(SUMO)
    entry = db["TRTL"]
    entry.insert_one(TRTL)
    entry = db["WAE"]
    entry.insert_one(WAE)
    entry = db["XAO"]
    entry.insert_one(XAO)
    entry = db["XMR"]
    entry.insert_one(XMR)
    entry = db["XTL"]
    entry.insert_one(XTL)
    entry = db["XUN"]
    entry.insert_one(XUN)
    entry = db["XVG"]
    entry.insert_one(XVG)
   # entry.collection.insert_Many(AEON,BTCP,COAL,DASH,DNR,DOGE,ETN,FBF,FBF,GRFT,IPBC,IRD,ITNS,KRB,LTC,LUX,NAH,NBR,PCN,PLURA,SUMO,TRTL,WAE,XAO,XMR,XTL,XVG])

    

if __name__ == '__main__':
    main()